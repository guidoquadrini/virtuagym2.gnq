<?php

header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");

include_once '../configs/database.php';
include_once '../objects/user_plan.php';

$method = $_SERVER['REQUEST_METHOD'];

$rest = new rest_user_plan();

//Parse payload from the request to get the vars.
parse_str(file_get_contents("php://input"), $post_vars);

//TODO: Data valitation

switch ($method) {
    case 'PUT':
        $rest->PUT($post_vars);
        break;
    case 'POST':
        $rest->POST($post_vars);
        break;
    case 'GET':
        $rest->GET($_GET);
        break;
    case 'DELETE':
        $rest->DELETE($post_vars);
        break;
    default:
        die('NOT VALID METHOD.');
        break;
}

class rest_user_plan {

    private $db;

    public function __construct() {
        $database = new Database();
        $this->db = $database->getConnection();
    }

    public function GET($data) {//READ
        $user_plan = new User_plan($this->db);
        
        if (!empty($data)) {
            if (isset($data['notin_plans_id'])) {
                $user_plan->plans_id = $data['notin_plans_id'];
                $stmt = $user_plan->get_UsersNotInPlan();
            } else if (isset($data['in_plans_id'])) {
                $user_plan->plans_id = $data['in_plans_id'];
                $stmt = $user_plan->get_UsersInPlan();
            }else {
                $user_plan->id = $data['id'];
                $stmt = $user_plan->read();
            }
        } else {
            $stmt = $user_plan->read();
        }

        $num = $stmt->rowCount();

        if ($num > 0) {

            $items_arr = array();
            $items_arr["data"] = array();

            while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
                // extract row, this will make $row['name'] to just $name only

                array_push($items_arr["data"], $row);
            }

            http_response_code(200);
            echo json_encode(["state" => "OK", "data" => $items_arr['data']]);
        } else {

            http_response_code(404);
            echo json_encode(["state" => "NOK", "message" => "No records found."]);
        }
    }

    public function POST($data) {//CREATE
        $user_plan = new User_plan($this->db);

        $user_plan->users_id = $data['users_id'];
        $user_plan->plans_id = $data['plans_id'];

        try {
            $user_plan->save();
            http_response_code(200);
            echo json_encode(["state" => "CREATED", "data" => $user_plan]);
        } catch (Exception $exc) {
            http_response_code(404);
            echo json_encode(["state" => "NOK", "message" => $exc->getTraceAsString()]);
            echo $exc->getTraceAsString();
        }
    }

    public function PUT($data) {//UPDATE
        $user_plan_plan = new User_plan($this->db);

        $user_plan->id = $data['id'];
        $user_plan->users_id = $data['users_id'];
        $user_plan->plans_id = $data['plans_id'];

        try {
            $user_plan->save();
            http_response_code(200);
            echo json_encode(["state" => "UPDATED", "data" => $user_plan]);
        } catch (Exception $exc) {
            http_response_code(404);
            echo json_encode(["state" => "NOK", "message" => $exc->getTraceAsString()]);
            echo $exc->getTraceAsString();
        }
    }

    public function DELETE($data) {//DELETE
        $user_plan = new User_plan($this->db);
        
        $user_plan->id = $data['id'];

        try {
            $user_plan->delete();
            http_response_code(200);
            echo json_encode(["state" => "DELETED"]);
        } catch (Exception $exc) {
            http_response_code(404);
            echo json_encode(["state" => "NOK", "message" => $exc->getTraceAsString()]);
            echo $exc->getTraceAsString();
        }
    }

}
