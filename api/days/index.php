<?php

header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");

include_once '../configs/database.php';
include_once '../objects/day.php';

$method = $_SERVER['REQUEST_METHOD'];

$rest = new rest_day();

//Parse payload from the request to get the vars.
parse_str(file_get_contents("php://input"), $post_vars);

//TODO: Data valitation

switch ($method) {
    case 'PUT':
        $rest->PUT($post_vars);
        break;
    case 'POST':
        $rest->POST($post_vars);
        break;
    case 'GET':
        $rest->GET($_GET);
        break;
    case 'DELETE':
        $rest->DELETE($post_vars);
        break;
    default:
        die('NOT VALID METHOD.');
        break;
}

class rest_day {

    private $db;

    public function __construct() {
        $database = new Database();
        $this->db = $database->getConnection();
    }

    public function GET($data) {//READ
        
        $day = new Day($this->db);

        if (!empty($data)) {
            $day->id = $data['id'];
            $stmt = $day->read();
        } else {
            $stmt = $day->read();
        }

        $num = $stmt->rowCount();

        if ($num > 0) {

            $items_arr = array();
            $items_arr["data"] = array();

            while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
                // extract row, this will make $row['name'] to just $name only
                extract($row);

                $item = [
                    "id" => $id,
                    "days_name" => $days_name
                ];

                array_push($items_arr["data"], $item);
            }

            http_response_code(200);
            echo json_encode(["state" => "OK", "data" => $items_arr['data']]);
        } else {

            http_response_code(404);
            echo json_encode(["state" => "NOK", "message" => "No records found."]);
        }
    }

    public function POST($data) {//CREATE
        $day = new Day($this->db);

        $day->days_name = $data['days_name'];

        try {
            $day->save();
            http_response_code(200);
            echo json_encode(["state" => "CREATED", "data" => $day]);
        } catch (Exception $exc) {
            http_response_code(404);
            echo json_encode(["state" => "NOK", "message" => $exc->getTraceAsString()]);
            echo $exc->getTraceAsString();
        }
    }

    public function PUT($data) {//UPDATE
        $day = new Day($this->db);

        $day->id = $data['id'];
        $day->days_name = $data['days_name'];

        try {
            $day->save();
            http_response_code(200);
            echo json_encode(["state" => "UPDATED", "data" => $day]);
        } catch (Exception $exc) {
            http_response_code(404);
            echo json_encode(["state" => "NOK", "message" => $exc->getTraceAsString()]);
            echo $exc->getTraceAsString();
        }
    }

    public function DELETE($data) {//DELETE
        $day = new Day($this->db);
        
        $day->id = $data['id'];

        try {
            $day->delete();
            http_response_code(200);
            echo json_encode(["state" => "DELETED"]);
        } catch (Exception $exc) {
            http_response_code(404);
            echo json_encode(["state" => "NOK", "message" => $exc->getTraceAsString()]);
            echo $exc->getTraceAsString();
        }
    }

}
