<?php

class Excercise {

    private $conn;
    private $table_name = "excercises";
    public $id;
    public $excercises_name;
    public $description;

    // constructor with $db as database connection
    public function __construct($db) {
        $this->conn = $db;
    }
    
    
    function read() {
        
        $query = "SELECT * FROM " . $this->table_name;
        
        if (!empty($this->id)){
            $query .= " WHERE id =  " . $this->id;
        }
        
            $query .= " ORDER BY excercises_name ASC";
            
        $stmt = $this->conn->prepare($query);
        
        $stmt->execute();
        
        return $stmt;
    }
    
    function save() {
        
        if (empty($this->id)){
            $query = " INSERT INTO " . $this->table_name . "(excercises_name, description) values( "
                    ." '" . $this->excercises_name . "', "
                    ." '" . $this->description . "' "
                    ." ); ";
        }else{
            $query = " UPDATE " . $this->table_name . " SET "
                    ." excercises_name= '" . $this->excercises_name     . "', "
                    ." description= '" . $this->description     . "' "
                    ." WHERE id= "    . $this->id            . ";";           
        }
        
        $stmt = $this->conn->prepare($query);
        $stmt->execute();
        //GET Last inserted id to return.
        return $stmt;
        
    }
    
    function delete() {      
        
        if (!empty($this->id)){
            $query = " DELETE FROM " . $this->table_name
                    ." WHERE id = " . $this->id . ";";
        }
        
        $stmt = $this->conn->prepare($query);
        $stmt->execute();
        
        return $stmt;
    }

}



