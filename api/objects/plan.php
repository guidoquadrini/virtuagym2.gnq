<?php
include_once('day.php');

class Plan {

    private $conn;
    private $table_name = "plans";
    public $id;
    public $plans_name;
    public $days;

    // constructor with $db as database connection
    public function __construct($db) {
        $this->conn = $db;
    }
      
    function read() {
        
        $query = "SELECT * FROM " . $this->table_name;
        
        if (!empty($this->id)){
            $query .= " WHERE id =  " . $this->id;
        }
        
            $query .= " ORDER BY plans_name ASC";
            
        $stmt = $this->conn->prepare($query);
        
        $stmt->execute();
        
        return $stmt;
    }
    
    function save() {
        
        if (empty($this->id)){
            $query = " INSERT INTO " . $this->table_name . "(plans_name) values( "
                    ." '" . $this->plans_name . "' "
                    ." ); ";
        }else{
            $query = " UPDATE " . $this->table_name . " SET "
                    ." first_name= '" . $this->plans_name    . "' "
                    ." WHERE id= "    . $this->id            . ";";           
        }
        
        $stmt = $this->conn->prepare($query);
        $stmt->execute();
        //GET Last inserted id to return.
        return $stmt;
        
    }
    
    function delete() {      
        
        if (!empty($this->id)){
            $query = " DELETE FROM " . $this->table_name
                    ." WHERE id = " . $this->id . ";";
        }
        
        $stmt = $this->conn->prepare($query);
        $stmt->execute();
        
        return $stmt;
    }

}



