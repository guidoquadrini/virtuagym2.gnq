<?php
include_once('excercise.php');

class Day {

    private $conn;
    private $table_name = "day";
    public $id;
    public $days_name;
    public $excercises;

    // constructor with $db as database connection
    public function __construct($db) {
        $this->conn = $db;
    }
    
    
    function read() {
        
        $query = "SELECT * FROM " . $this->table_name;
        
        if (!empty($this->id)){
            $query .= " WHERE id =  " . $this->id;
        }
        
            $query .= " ORDER BY days_name ASC";
            
        $stmt = $this->conn->prepare($query);
        
        $stmt->execute();
        
        return $stmt;
    }
    
    function save() {
        
        if (empty($this->id)){
            $query = " INSERT INTO " . $this->table_name . "(days_name) values( "
                    ." '" . $this->days_name . "', "
                    ." ); ";
        }else{
            $query = " UPDATE " . $this->table_name . " SET "
                    ." days_name= '" . $this->days_name     . "' "
                    ." WHERE id= "    . $this->id            . ";";           
        }
        
        $stmt = $this->conn->prepare($query);
        $stmt->execute();
        //GET Last inserted id to return.
        return $stmt;
        
    }
    
    function delete() {      
        
        if (!empty($this->id)){
            $query = " DELETE FROM " . $this->table_name
                    ." WHERE id = " . $this->id . ";";
        }
        
        $stmt = $this->conn->prepare($query);
        $stmt->execute();
        
        return $stmt;
    }

}



