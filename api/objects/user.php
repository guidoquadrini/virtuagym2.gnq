<?php

class User {

    private $conn;
    private $table_name = "users";
    public $id;
    public $firstname;
    public $lastname;
    public $email;

    // constructor with $db as database connection
    public function __construct($db) {
        $this->conn = $db;
    }
    
    
    function read() {
        
        $query = "SELECT * FROM " . $this->table_name;
        
        if (!empty($this->id)){
            $query .= " WHERE id =  " . $this->id;
        }
        
            $query .= " ORDER BY last_name ASC";
            
        $stmt = $this->conn->prepare($query);
        
        $stmt->execute();
        
        return $stmt;
    }
    
    function save() {
        
        if (empty($this->id)){
            $query = " INSERT INTO " . $this->table_name . "(first_name, last_name, email) values( "
                    ." '" . $this->firstname . "', "
                    ." '" . $this->lastname . "', "
                    ." '" . $this->email . "' "
                    ." ); ";
        }else{
            $query = " UPDATE " . $this->table_name . " SET "
                    ." first_name= '" . $this->firstname     . "', "
                    ." last_name= '"  . $this->lastname      . "', "
                    ." email= '"      . $this->email         . "' "
                    ." WHERE id= "    . $this->id            . ";";           
        }
        
        $stmt = $this->conn->prepare($query);
        $stmt->execute();
        //GET Last inserted id to return.
        return $stmt;
        
    }
    
    function delete() {      
        
        if (!empty($this->id)){
            $query = " DELETE FROM " . $this->table_name
                    ." WHERE id = " . $this->id . ";";
        }
        
        $stmt = $this->conn->prepare($query);
        $stmt->execute();
        
        return $stmt;
    }

}



