<?php

class User_plan {

    private $conn;
    private $table_name = "users_plans";
    public $id;
    public $users_id;
    public $plans_id;

    // constructor with $db as database connection
    public function __construct($db) {
        $this->conn = $db;
    }
    
    function get_UsersNotInPlan(){
        $query = " SELECT U1.id, U1.first_name, U1.last_name FROM users U1 "
                ." WHERE U1.id NOT IN ( SELECT U.id FROM users_plans UP "
                ." INNER JOIN users U ON U.id = UP.users_id "
                ." WHERE UP.plans_id = " . $this->plans_id . ") ";
        $query .= " ORDER BY U1.first_name ASC; ";
           
        $stmt = $this->conn->prepare($query);
        
        $stmt->execute();
        
        return $stmt;
    }
    
    function get_UsersInPlan(){
        $query = " SELECT UP.id, U.first_name, U.last_name FROM users_plans UP "
                ." INNER JOIN users U ON U.id = UP.users_id "
                ." WHERE UP.plans_id = " . $this->plans_id;
        $query .= " ORDER BY U.first_name ASC; ";
       
        $stmt = $this->conn->prepare($query);
        
        $stmt->execute();
        
        return $stmt;
    }
    
    function read() {
        
        $query = "SELECT * FROM " . $this->table_name;
        
        if (!empty($this->id)){
            $query .= " WHERE id =  " . $this->id;
        }
        
            $query .= " ORDER BY id ASC";
            
        $stmt = $this->conn->prepare($query);
        
        $stmt->execute();
        
        return $stmt;
    }
    
    function save() {
        
        if (empty($this->id)){
            $query = " INSERT INTO users_plans(users_id, plans_id) values( "
                    ." '" . $this->users_id . "', "
                    ." '" . $this->plans_id . "' "
                    ." ); ";
        }
        
        $stmt = $this->conn->prepare($query);
        $stmt->execute();
        $last_inserted_id = $this->conn->lastInsertId();
        
        $query = " SELECT UP.id, U.first_name, U.last_name FROM users_plans UP "
                ." INNER JOIN users U ON U.id = UP.users_id "
                ." WHERE UP.plans_id = " . $last_inserted_id;
        $stmt = $this->conn->prepare($query);
        $stmt->execute();
        
        return $stmt;
        
    }
    
    function delete() {      
        
        if (!empty($this->id)){
            $query = " DELETE FROM " . $this->table_name
                    ." WHERE id = " . $this->id . ";";
        }
        
        $stmt = $this->conn->prepare($query);
        $stmt->execute();
        
        return $stmt;
    }

}



