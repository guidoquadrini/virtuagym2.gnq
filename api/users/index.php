<?php

header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");

include_once '../configs/database.php';
include_once '../objects/user.php';

$method = $_SERVER['REQUEST_METHOD'];

$rest = new rest_user();

//Parse payload from the request to get the vars.
parse_str(file_get_contents("php://input"), $post_vars);

//TODO: Data valitation

switch ($method) {
    case 'PUT':
        $rest->PUT($post_vars);
        break;
    case 'POST':
        $rest->POST($post_vars);
        break;
    case 'GET':
        $rest->GET($_GET);
        break;
    case 'DELETE':
        $rest->DELETE($post_vars);
        break;
    default:
        die('NOT VALID METHOD.');
        break;
}

class rest_user {

    private $db;

    public function __construct() {
        $database = new Database();
        $this->db = $database->getConnection();
    }

    public function GET($data) {//READ
        
        $user = new User($this->db);

        if (!empty($data)) {
            $user->id = $data['id'];
            $stmt = $user->read();
        } else {
            $stmt = $user->read();
        }

        $num = $stmt->rowCount();

        if ($num > 0) {

            $items_arr = array();
            $items_arr["data"] = array();

            while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
                // extract row, this will make $row['name'] to just $name only
                extract($row);

                $item = [
                    "id" => $id,
                    "firstname" => $first_name,
                    "lastname" => $last_name,
                    "email" => $email
                ];

                array_push($items_arr["data"], $item);
            }

            http_response_code(200);
            echo json_encode(["state" => "OK", "data" => $items_arr['data']]);
        } else {

            http_response_code(404);
            echo json_encode(["state" => "NOK", "message" => "No records found."]);
        }
    }

    public function POST($data) {//CREATE
        $user = new User($this->db);

        $user->firstname = $data['firstname'];
        $user->lastname = $data['lastname'];
        $user->email = $data['email'];

        try {
            $user->save();
            http_response_code(200);
            echo json_encode(["state" => "CREATED", "data" => $user]);
        } catch (Exception $exc) {
            http_response_code(404);
            echo json_encode(["state" => "NOK", "message" => $exc->getTraceAsString()]);
            echo $exc->getTraceAsString();
        }
    }

    public function PUT($data) {//UPDATE
        $user = new User($this->db);

        $user->id = $data['id'];
        $user->firstname = $data['firstname'];
        $user->lastname = $data['lastname'];
        $user->email = $data['email'];

        try {
            $user->save();
            http_response_code(200);
            echo json_encode(["state" => "UPDATED", "data" => $user]);
        } catch (Exception $exc) {
            http_response_code(404);
            echo json_encode(["state" => "NOK", "message" => $exc->getTraceAsString()]);
            echo $exc->getTraceAsString();
        }
    }

    public function DELETE($data) {//DELETE
        $user = new User($this->db);
        
        $user->id = $data['id'];

        try {
            $user->delete();
            http_response_code(200);
            echo json_encode(["state" => "DELETED"]);
        } catch (Exception $exc) {
            http_response_code(404);
            echo json_encode(["state" => "NOK", "message" => $exc->getTraceAsString()]);
            echo $exc->getTraceAsString();
        }
    }

}
