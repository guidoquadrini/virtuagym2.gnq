
DROP DATABASE IF EXISTS db_virtuagym2_gnq;
CREATE DATABASE db_virtuagym2_gnq;
USE db_virtuagym2_gnq;


SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for days
-- ----------------------------
DROP TABLE IF EXISTS `days`;
CREATE TABLE `days` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `days_name` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of days
-- ----------------------------

-- ----------------------------
-- Table structure for days_exercises
-- ----------------------------
DROP TABLE IF EXISTS `days_exercises`;
CREATE TABLE `days_exercises` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `days_id` int(11) DEFAULT NULL,
  `exercises_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_days_id` (`days_id`),
  KEY `fk_exercises_id` (`exercises_id`),
  CONSTRAINT `fk_days_id` FOREIGN KEY (`days_id`) REFERENCES `days` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_exercises_id` FOREIGN KEY (`exercises_id`) REFERENCES `exercises` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of days_exercises
-- ----------------------------

-- ----------------------------
-- Table structure for exercises
-- ----------------------------
DROP TABLE IF EXISTS `exercises`;
CREATE TABLE `exercises` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `excercises_name` varchar(100) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of exercises
-- ----------------------------

-- ----------------------------
-- Table structure for plans
-- ----------------------------
DROP TABLE IF EXISTS `plans`;
CREATE TABLE `plans` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `plans_name` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of plans
-- ----------------------------
INSERT INTO `plans` VALUES ('2', 'UpperBody');
INSERT INTO `plans` VALUES ('8', 'Hombros y Brazos');
INSERT INTO `plans` VALUES ('10', 'chochcoh');

-- ----------------------------
-- Table structure for plans_days
-- ----------------------------
DROP TABLE IF EXISTS `plans_days`;
CREATE TABLE `plans_days` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `plans_id` int(11) DEFAULT NULL,
  `days_name` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_plans_days_id` (`plans_id`),
  CONSTRAINT `fk_plans_days_id` FOREIGN KEY (`plans_id`) REFERENCES `plans` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of plans_days
-- ----------------------------

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(100) DEFAULT NULL,
  `last_name` varchar(100) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `username` varchar(100) DEFAULT NULL,
  `password` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO `users` VALUES ('5', 'Lisa', 'Mona', 'mona@lisa.com', null, null);
INSERT INTO `users` VALUES ('10', 'Mike', 'Thomas', 'mthomas@gmail.com', null, null);

-- ----------------------------
-- Table structure for users_plans
-- ----------------------------
DROP TABLE IF EXISTS `users_plans`;
CREATE TABLE `users_plans` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `users_id` int(11) DEFAULT NULL,
  `plans_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_users_id` (`users_id`),
  KEY `fk_plans_id` (`plans_id`),
  CONSTRAINT `fk_plans_id` FOREIGN KEY (`plans_id`) REFERENCES `plans` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_users_id` FOREIGN KEY (`users_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of users_plans
-- ----------------------------
INSERT INTO `users_plans` VALUES ('2', '5', '2');
INSERT INTO `users_plans` VALUES ('10', '5', '8');
INSERT INTO `users_plans` VALUES ('13', '10', '10');
