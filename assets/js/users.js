var user_url = "http://dwww.virtuagym2.com/api/users/";

$(document).ready(function () {

    load_users();

    $("#btn_user_add").click(function () {
        formUserFields_behavior('add');
        $("#modal_user").modal("show");
    });

    $("#btn_formUser_edit").click(function () {
        formUserFields_behavior('edit');
    });

    //SAVE Data From User Form
    $("#btn_formUser_save").click(function () {
        if ($('#form_user')[0].checkValidity() === true) {
            var data = {
                'id': $("#user_id").val(),
                'firstname': $("#firstname").val(),
                'lastname': $("#lastname").val(),
                'email': $("#email").val()
            };
            var ajax_method = "PUT"; //If it's a new user record, ajax                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                          use POST method to create.
            if (data.id === "") {
                ajax_method = "POST";
            }
            $.ajax({
                method: ajax_method,
                url: user_url,
                data: data,
                dataType: "json"
            }).done(function (response) {
                if (response.state === 'CREATED') {
                    card = create_a_card(response.data.id, response.data.firstname + " " + response.data.lastname);
                    $(".user_grid").append(card);
                } else if (response.state === 'UPDATED') {
                    $(".card").each(function (index, item) {
                        if ($(item).attr('data-id') == data.id) {
                            $(item).children("div.card-header").children("H4").text(data.firstname + " " + data.lastname);
                        }
                    })
                } else {
                    alert(response.message);
                }
                $("#modal_user").modal("hide");
            });
        }
    });

});

$(document).on('click', ".btn_user_delete", function () {
    var data = {'id': $(this).closest('.card').attr('data-id')};
    $.ajax({
        method: "DELETE",
        url: user_url,
        data: data,
        dataType: "json"
    }).done(function (response) {
        if (response.state === 'DELETED') {
            $(".card").each(function (index, item) {
                if ($(item).attr('data-id') == data.id) {
                    $(item).remove();
                        }
                    })
        } else {
            alert(response.message);
        }
    });
});

$(document).on('click', ".btn_user_view", function () {
    formUserFields_behavior('view');
    item_id = $(this).closest('.card').attr('data-id');
    $.ajax({
        method: 'GET',
        url: user_url + "?id=" + item_id,
        dataType: "json"
    }).done(function (response) {
        console.log(response);
        if (response.state === 'OK') {
            $("#form_user #firstname").val(response.data[0].firstname);
            $("#form_user #lastname").val(response.data[0].lastname);
            $("#form_user #email").val(response.data[0].email);
            $("#form_user #user_id").val(response.data[0].id);
            $("#modal_user").modal("show");
        } else {
            alert(response.message);
        }
    });
});

$(document).on('submit', '#form_user', function (e) {
    e.preventDefault();
    return false;
});

/***
 * Clean the fields in the form user.
 * @returns {undefined}
 */
function formUserFields_clean() {
    $("#form_user #firstname").val("");
    $("#form_user #lastname").val("");
    $("#form_user #email").val("");
    $("#form_user #user_id").val("");
}

/***
 * This function change the behavior of the form. 
 * ADD: Clean & Enable fields. Buttons: Save show; Edit hide;
 * VIEW: Disable fields. Buttons: Save hide; Modify show;
 * EDIT: Enable fields. Buttons: Save show; Modify hide;
 * @param {type} mode [add|view] 
 * @returns {undefined}
 */

function formUserFields_behavior(mode) {
    switch (mode) {
        case "add":
            formUserFields_clean();
            $("#label-behavior").text("Add");
            $("#form_user #firstname").prop('disabled', false);
            $("#form_user #lastname").prop('disabled', false);
            $("#form_user #email").prop('disabled', false);
            $("#btn_formUser_save").show();
            $("#btn_formUser_edit").hide();
            break;
        case "view":
            $("#label-behavior").text("View");
            $("#form_user #firstname").prop('disabled', true);
            $("#form_user #lastname").prop('disabled', true);
            $("#form_user #email").prop('disabled', true);
            $("#btn_formUser_save").hide();
            $("#btn_formUser_edit").show();
            break;
        case "edit":
            $("#label-behavior").text("Edit ");
            $("#form_user #firstname").prop('disabled', false);
            $("#form_user #lastname").prop('disabled', false);
            $("#form_user #email").prop('disabled', false);
            $("#btn_formUser_edit").hide();
            $("#btn_formUser_save").show();
            break;
    }

}

function load_users() {

    $.ajax({
        method: 'GET',
        url: user_url,
        data: "",
        dataType: "json"
    }).done(function (response) {

        if (response.data.length > 0) {
            $.each(response.data, function (index, item) {
                card = create_a_card(item.id, item.firstname + " " + item.lastname);
                $(".user_grid").append(card);
            });

        } else {
            $(".card-deck").html('No users.');
        }
    });



}

function create_a_card(id, name) {
    var card = card = '<div class="card col-12 col-sm-12 col-md-6 col-lg-3 col-xl-3 mb-3 s hadow-sm" ' +
            'data-id="' + id + '">' +
            '<div class="card-header">' +
            '<h4 class="my-0  font-weight-normal label-name">' + name + '</h4>' +
            '</div>' +
            '<div class="card-body">' +
            '<img class="rounded-circle" src="img/face/lisa.jpg"/>' +
            '<br><br>' +
            '<button type="button" class="btn_user_view btn btn-lg btn-block btn-info">View Details</button>' +
            '<button type="button" class="btn_plan_asign btn btn-lg btn-block btn-success"' +
            '<button type="button" class="btn_user_delete btn btn-lg btn-block btn-danger">Delete</button>' +
            '</div>' +
            '</div>';
    return card;
}

