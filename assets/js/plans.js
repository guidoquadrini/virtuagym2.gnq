var url = "http://dwww.virtuagym2.com/api/plans/";

$(document).ready(function () {

    load_plans();

    $("#btn_plan_add").click(function () {
        formPlanFields_behavior('add');
        $("#modal_plan").modal("show");
    });

    //SAVE Data From Plan Form
    $("#btn_formPlan_save").click(function () {
        if ($('#form_plan')[0].checkValidity() === true) {
            var data = {
                'plans_name': $("#plans_name").val()
            };

            $.ajax({
                method: 'POST',
                url: url,
                data: data,
                dataType: "json"
            }).done(function (response) {
                if (response.state === 'CREATED') {
                    card = create_a_card(response.data.id, response.data.plans_name);
                    $(".plan_grid").append(card);
                } else {
                    alert(response.message);
                }
                $("#modal_plan").modal("hide");
            });
        }
    });

    //Add a user to the pan.
    $("#btn_agregar_usuario").click(function () {

        var user_plan_url = "http://dwww.virtuagym2.com/api/users_plans/";
        data = {
            'users_id': $("#cbo_formUserPlan_users").val(),
            'plans_id': $("#fld_formUserPlan_plans_name").attr('data-plans_id')
        };
        $.ajax({
            method: 'POST',
            url: user_plan_url,
            data: data,
            dataType: "json"
        }).done(function (response) {
            if (response.state === 'CREATED') {
                alert('User Linked to Plan')
                window.location.reload()
                
                //TODO: refresh info without refresh    
                $('#boxuser ul').append(
                        '<li class="list-group-item d-flex justify-content-between align-items-center">' +
                        reponse.data.first_name + " " + response.data.last_name +
                        ' <span><button data-users_plans_id="' + response.data.id +
                        '" class="btn_remove_user_plan btn btn-danger btn-sm">Remove</button></span>' +
                        '</li>'
                        );   
            } else {
                alert(response.message);
            } 
        });
    });
});

//Get info of linked and unlinked users to complete the form.
$(document).on('click', ".btn_plan_asign", function () {
    $('#boxuser ul').html("");

    var plan = {
        'id': $(this).closest('.card').attr('data-id'),
        'plans_name': $(this).closest("div.card-body").siblings("div.card-header").children("H4").text()
    };
    $("#fld_formUserPlan_plans_name").attr('data-plans_id', plan.id);
    $("#fld_formUserPlan_plans_name").val(plan.plans_name);

    var user_plan_url = "http://dwww.virtuagym2.com/api/users_plans/";
    
    //Users no linked yet.
    $.ajax({
        method: 'GET',
        url: user_plan_url + "?notin_plans_id=" + plan.id,
        dataType: "json"
    }).done(function (response) {

        if (response.state === 'OK') {
            $('#cbo_formUserPlan_users').html('<option>Add User...</optino>');
            $.each(response.data, function (index, item) {
                $('#cbo_formUserPlan_users').append("<option value=" + item.id + ">"
                        + item.first_name + " " + item.last_name + "</option>");
            });
            $("#modal_user_plan").modal('show');
        } else {
            alert(response.message);
        }
    });
    //Users Linked to Plan
    $.ajax({
        method: 'GET',
        url: user_plan_url + "?in_plans_id=" + plan.id,
        dataType: "json"
    }).done(function (response) {
        if (response.state === 'OK') {
            $('#boxuser ul').html("");
            $.each(response.data, function (index, item) {
                $('#boxuser ul').append(
                        '<li class="list-group-item d-flex justify-content-between align-items-center">' +
                        item.first_name + " " + item.last_name +
                        ' <span><button data-users_plans_id="' + item.id +
                        '" class="btn_remove_user_plan btn btn-danger btn-sm">Remove</button></span>' +
                        '</li>'
                        );
            });

            $("#modal_user_plan").modal('show');
        } else if (response.state === 'NOK') {
            $('#boxuser ul').html("");
        } else {
            alert(response.message);
        }
    });



});

//Delete a simple plan.
$(document).on('click', ".btn_plan_delete", function () {
    var data = {'id': $(this).closest('.card').attr('data-id')};
    $.ajax({
        method: "DELETE",
        url: url,
        data: data,
        dataType: "json"
    }).done(function (response) {
        if (response.state === 'DELETED') {
            $(".card").each(function (index, item) {
                if ($(item).attr('data-id') == data.id) {
                    $(item).remove();
                }
            })
        } else {
            alert(response.message);
        }
    });
});

$(document).on("click", ".btn_remove_user_plan", function () {
    var user_plan_url = "http://dwww.virtuagym2.com/api/users_plans/";
    var data = {
        'id': $(this).attr('data-users_plans_id')
    }

    $.ajax({
        method: "DELETE",
        url: user_plan_url,
        data: data,
        dataType: "json"
    }).done(function (response) {
        if (response.state === 'DELETED') {
            $("#boxuser ul li").each(function (index, item) {
                if ($(item).children('span').children('button').attr('data-users_plans_id') == data.id) {
                    $(item).remove();
                }
            })
        } else {
            alert(response.message);
        }
    });
});

$(document).on('click', ".btn_plan_view", function () {
    $("#modal_plandetails").modal("show");return false;
    plandetails_url = "http://dwww.virtuagym2.com/api/plans/";
    formUserFields_behavior('view');
    item_id = $(this).closest('.card').attr('data-id');
    $.ajax({
        method: 'GET',
        url: plandetails_url + "?id=" + item_id,
        dataType: "json"
    }).done(function (response) {
        console.log(response);
        if (response.state === 'OK') {
            load_plandetails()
            $("#modal_plandetails").modal("show");
        } else {
            alert(response.message);
        }
    });
});

$(document).on('submit', '#form_plan,#form_user_plan', function (e) {
    e.preventDefault();
    return false;
});

/***
 * Clean the fields in the form user.
 * @returns {undefined}
 */
function formPlanFields_clean() {
    $("#plans_id").val("");
    $("#plana_name").val("");
}

/***
 * This function change the behavior of the form. 
 * ADD: Clean & Enable fields. Buttons: Save show; Edit hide;
 * VIEW: Disable fields. Buttons: Save hide; Modify show;
 * EDIT: Enable fields. Buttons: Save show; Modify hide;
 * @param {type} mode [add|view] 
 * @returns {undefined}
 */

function formPlanFields_behavior(mode) {
    switch (mode) {
        case "add":
            formPlanFields_clean();
            $("#label-behavior").text("Add");
            $("#plans_name").prop('disabled', false);
            $("#btn_formUser_save").show();
            break;
    }

}

function load_plans() {

    $.ajax({
        method: 'GET',
        url: url,
        data: "",
        dataType: "json"
    }).done(function (response) {

        if (response.data.length > 0) {
            $.each(response.data, function (index, item) {
                card = create_a_card(item.id, item.plans_name);
                $(".plan_grid").append(card);
            });

        } else {
            $(".plan_grid").html('No users.');
        }
    });
}

function load_plandetails() {

}

function create_a_card(id, name) {
    var card = card = '<div class="card col-12 col-sm-12 col-md-6 col-lg-3 col-xl-3 mb-3 s hadow-sm" ' +
            'data-id="' + id + '">' +
            '<div class="card-header">' +
            '<h4 class="my-0  font-weight-normal label-name">' + name + '</h4>' +
            '</div>' +
            '<div class="card-body">' +
            
            '<button type="button" class="btn_plan_view btn btn-lg btn-block btn-info">View Details</button>' +
            '<button type="button" class="btn_plan_asign btn btn-lg btn-block btn-success">Asign Plan</button>' +
            '<button type="button" class="btn_plan_delete btn btn-lg btn-block btn-danger">Delete</button>' +
            '</div>' +
            '</div>';
    return card;
}

