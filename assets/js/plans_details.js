var url = "http://dwww.virtuagym2.com/api/plans/";

$(document).ready(function () {

    $("#form_userdetails_plan_title").click(function () {
        formPlanFields_behavior('add');
        $("#modal_plan").modal("show");
    });

    $("#form_userdetails_plan_title_icon").click(function () {
        icon = $("#form_userdetails_plan_title_icon").children("i");
        console.log(icon.hasClass("fa-edit"))
        if (icon.hasClass("fa-edit") === true) {
            icon.removeClass("fa-edit");
            icon.addClass("fa-save");
            $("#form_userdetails_plan_title").attr("disabled", false);
        } else {
            //Make Ajax Call to Save the plan's name.
            //If its ok then go on with the next lines...
            icon.removeClass("fa-save");
            icon.addClass("fa-edit");
            $("#form_userdetails_plan_title").attr("disabled", true);
        }
    });

    $("#form_userdetails_cbo_days").focus(function () {
        cargar_dias();
    });

    /** Add Excercise to a day **/
    $("#btn_add_excercise").click(function () {
        if ($('#form_userdetails_cbo_days')[0].checkValidity() === true) {
            var day = 99;//$("#form_userdetails_cbo_days").val();
            var excercise = {'id': '999', 'name': 'Tango Sureño'};

            var days_grid = $(".day");
            $.each(days_grid, function (index, item) {

                ///console.log($(item).find("span").attr("data-id"));

                if ($(item).children("div.day_title").children("h5").children("span").attr("data-id") == 999) {
                    console.log(item)
                    $(item).children("ul.list-group").append(
                            '<li data-excersice-id="999" class="list-group-item d-flex justify-content-between' +
                            ' align-items-center">' + excercise.name +
                            '<span data-day_extercise-id="888" title="Remove excercise" class="btn btn-sm' +
                            ' btn-danger">' +
                            '<i alt="X" class="fas fa-times"></i>' +
                            '</span>' +
                            '</li>');
                }
            });


        }
    });

    /** Add day button **/
    $("#btn_add_day").click(function () {
        //Make ajax request to insert the data, control the response... next OK response
        day_id = 1; //Get from the inserted item.
        var new_day =   '<div class="day col-4 mt-3">' +
                           '<div class="input-group mb-3 day_title">'+
                                '<h5 class="dayname"><span data-id="' + day_id + '">No name</span>' +
                                    '<div class="btn-group">'+
                                        '<button type="button" class="btn_form_userdetails_dayname_edit btn-sm btn-light"><i class="fas fa-edit"></i></button>      '+
                                    '</div>'+
                                '</h5>'+
                            '</div>'+
                            '<ul class="list-group"></ul>' +
                        '</div>';
        
        var days_grid = $("div.row.days");
        days_grid.append(new_day);
    });

});

/** Prevent form submit **/
$(document).on('submit', '#form_add_excercise', function (e) {
    e.preventDefault();
    return false;
});

/** Remove excercise button behavior **/
$(document).on('click', '.btn_form_userdetails_excercise_remove', function () {
    var excercise_id = $(this).closest("li").attr("data-excercise-od");
    var day_id = $(this).closest(".day").children("h5").children("span").attr("data-id");
    //Ajax Call to remove the excercise... If OK... remove the tag.
    $(this).closest("li").remove();

});

/** BIGIN of Day Plan Title Controls **/

/** Title Edit Button **/
$(document).on('click', '.btn_form_userdetails_dayname_edit', function () {
    $("#form_userdetails_cbo_excercises,#form_userdetails_cbo_days,#btn_add_excercise").attr("disabled", true);
    $(this).closest(".day_title").html(
            '<input type="text" class="form-control" placeholder="Days\'s name" value="Day Upper Legs"> ' +
            ' <div class="input-group-append">' +
            '<div class="btn-group">' +
            '<button type="button" class="btn_form_userdetails_dayname_save btn btn-success"><i class="fas fa-save"></i></button>' +
            '<button type="button" class="btn_form_userdetails_dayname_delete btn btn-danger"><i class="fas fa-times"></i></button>' +
            '</div>' +
            '</div>');

});

/** Title Save Button **/
$(document).on('click', '.btn_form_userdetails_dayname_save', function () {
    $(this).closest(".day_title").html(
            '<h5 class="dayname"><span data-id="999">Day Upper Arms </span> ' +
            '<div class="btn-group" role="group" aria-label="Third group"> ' +
            '<button type="button" class="btn_form_userdetails_dayname_edit btn-sm btn-light">' +
            '<i class="fas fa-edit"></i>' +
            '</button>' +
            '</div>' +
            '</h5>');
    $("#form_userdetails_cbo_excercises,#form_userdetails_cbo_days,#btn_add_excercise").attr("disabled", false);
});

/** Title Delete Button **/
$(document).on('click', '.btn_form_userdetails_dayname_delete', function () {
    $(this).closest(".day").remove();
});
/** END of Day Plan Title Controls **/




/**
 * Find days rendered with their ids. 
 * @returns {String} a string of html options tags.
 * 
 */
function cargar_dias() {
    var days = $("h5.dayname span");
    var options = "<option selected>Choose Day...</option>";
    $.each(days, function (index, item) {
        options += "<option value='" + $(item).attr("data-id") + "' selected>" + $(item).text() + "</option>";
    });
    $("#form_userdetails_cbo_days").html(options);

}
        